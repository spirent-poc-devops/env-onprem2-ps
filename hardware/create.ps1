#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

############################## Keypair #####################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Initialize terraform
Set-Location $keypairTerraformPath
Write-Host "Initializing terraform..."
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "hw_keypair" "Can't initialize terraform. Watch logs above or check './temp/$keypairTerraformPath' folder content"
}

# Plan cloud resources creation
Write-Host "Executing terraform plan command..."
terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "hw_keypair" "Can't execute terraform plan. Watch logs above or check './temp/$keypairTerraformPath' folder content"
}

# Create cloud resources
Write-Host "Creating cloud resources by terraform..."
terraform apply -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "hw_keypair" "Can't create cloud resources. Watch logs above or check './temp/$keypairTerraformPath' folder content"
}

# Get keypair name
$keypair=$(terraform output keypair_name).Trim('"')
Set-EnvMapValue -Map $resources -Key "hw.k8s.keypair_name" -Value $keypair
Set-EnvMapValue -Map $resources -Key "hw.mgmtstation.keypair_name" -Value $keypair

if (!(Test-Path "$PSScriptRoot/../config/$keypair.pem")) {
    Copy-Item -Path "$keypairTerraformPath/secrets/$keypair.pem" -Destination "$PSScriptRoot/../config/" -Recurse
}

Set-Location "$PSScriptRoot/.."

# Save terraform state to config folder
Save-State -Component "keypair" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
###################################################################

################################## Kubernetes Virtual Machines #################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources
       
# Initialize terraform
Set-Location $k8sTerraformPath
Write-Host "Initializing terraform..."
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't initialize terraform. Watch logs above or check '$k8sTerraformPath' folder content."
}

# Plan cloud resources creation
Write-Host "Executing terraform plan command..."
terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't execute terraform plan. Watch logs above or check '$k8sTerraformPath' folder content."
}

# Create cloud resources
Write-Host "Creating cloud resources by terraform..."
terraform apply -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't create cloud resources. Watch logs above or check '$k8sTerraformPath' folder content."
}

for ($i = 0; $i -lt $k8sVMCount; $i++) {
    # Save results to resource file
    $k8sId = $(terraform output "k8s_$i`_id").Trim('"')
    $k8sPrivateIp = $(terraform output "k8s_$i`_private_ip").Trim('"') 
    $k8sPublicIp = $(terraform output "k8s_$i`_public_ip").Trim('"')
    $k8sSgId = $(terraform output "k8s_$i`_sg_id").Trim('"')
    
    Set-EnvMapValue -Map $resources -Key "hw.k8s.$i`_id" -Value $k8sId
    Set-EnvMapValue -Map $resources -Key "hw.k8s.$i`_private_ip" -Value $k8sPrivateIp
    Set-EnvMapValue -Map $resources -Key "hw.k8s.$i`_public_ip" -Value $k8sPublicIp
    Set-EnvMapValue -Map $resources -Key "hw.k8s.$i`_sg_id" -Value $k8sSgId
}

# Open access by public ip
$env:AWS_DEFAULT_REGION = Get-EnvMapValue -Map $config -Key "hw.aws.region"
aws ec2 authorize-security-group-ingress --group-id $k8sSgId --protocol tcp --port 0-65535 --cidr "$k8sPublicIp/32"

Set-Location "$PSScriptRoot/.."

# Save terraform state to config folder
Save-State -Component "k8s" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

# Save k8s nodes ip to config file
$masterIps = @()
$workerIps = @()
Set-EnvMapValue -Map $config -Key "k8s.username" -Value $(Get-EnvMapValue -Map $config -Key "hw.k8s.username")
Set-EnvMapValue -Map $config -Key "k8s.private_key_path" -Value "config/$(Get-EnvMapValue -Map $resources -Key "hw.k8s.keypair_name").pem"
for ($i = 0; $i -lt $(Get-EnvMapValue -Map $config -Key "hw.k8s.masters_count"); $i++) {
    $masterIps += (Get-EnvMapValue -Map $resources -Key "hw.k8s.$i`_private_ip")
}
for ($i = $(Get-EnvMapValue -Map $config -Key "hw.k8s.masters_count"); $i -lt $k8sVMCount; $i++) {
    $workerIps += (Get-EnvMapValue -Map $resources -Key "hw.k8s.$i`_private_ip")
}
Set-EnvMapValue -Map $config -Key "k8s.master_ips" -Value $masterIps
Set-EnvMapValue -Map $config -Key "k8s.worker_ips" -Value $workerIps
Write-EnvResources -ResourcePath $ConfigPath -Resources $config

# $configOnprem = Get-Content -Path $ConfigPath | Out-String | ConvertFrom-Json 
# $configOnprem.k8s | add-member -Name "master_ips" -Value @() -MemberType NoteProperty -Force
# $configOnprem.k8s | add-member -Name "worker_ips" -Value @() -MemberType NoteProperty -Force
# $configOnprem.k8s | add-member -Name "username" -Value (Get-EnvMapValue -Map $config -Key "hw.k8s.username") -MemberType NoteProperty -Force
# $configOnprem.k8s | add-member -Name "private_key_path" -Value "config/$(Get-EnvMapValue -Map $resources -Key "hw.k8s.keypair_name").pem" -MemberType NoteProperty -Force
# for ($i = 0; $i -lt $(Get-EnvMapValue -Map $config -Key "hw.k8s.masters_count"); $i++) {
#     $configOnprem.k8s."master_ips" += (Get-EnvMapValue -Map $resources -Key "hw.k8s.$i`_private_ip")
# }
# for ($i = $(Get-EnvMapValue -Map $config -Key "hw.k8s.masters_count"); $i -lt $k8sVMCount; $i++) {
#     $configOnprem.k8s."worker_ips" += (Get-EnvMapValue -Map $resources -Key "hw.k8s.$i`_private_ip")
# }
# ConvertTo-Json $configOnprem | Set-Content -Path $ConfigPath

###################################################################

############################### k8s loadbalancer ####################################

if ($(Get-EnvMapValue -Map $config -Key "hw.k8s.masters_count") -gt 1) {
    # Prepare terraform files and set terraform env vars
    . "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

    # Initialize terraform
    Set-Location $k8sLBTerraformPath
    Write-Host "Initializing terraform..."
    terraform init
    # Check for error
    if ($LastExitCode -ne 0) {
        Set-Location "$PSScriptRoot/.."
        Write-EnvError -Component "kubernetes_lb" "Can't initialize terraform. Watch logs above or check '$k8sLBTerraformPath' folder content."
    }

    # Plan cloud resources creation
    Write-Host "Executing terraform plan command..."
    terraform plan
    # Check for error
    if ($LastExitCode -ne 0) {
        Set-Location "$PSScriptRoot/.."
        Write-EnvError -Component "kubernetes_lb" "Can't execute terraform plan. Watch logs above or check '$k8sLBTerraformPath' folder content."
    }

    # Create cloud resources
    Write-Host "Creating cloud resources by terraform..."
    terraform apply -auto-approve
    # Check for error
    if ($LastExitCode -ne 0) {
        Set-Location "$PSScriptRoot/.."
        Write-EnvError -Component "kubernetes_lb" "Can't create cloud resources. Watch logs above or check '$k8sLBTerraformPath' folder content."
    }

    # Create k8s loadbalancer outputs to resources
    Set-EnvMapValue -Map $resources -Key "hw.k8s.lb_state" -Value "deployed"
    $k8s_lb_id=$(terraform output k8s_lb_id).Trim('"')
    $k8s_lb_private_ip=$(terraform output k8s_lb_private_ip).Trim('"')
    $k8s_lb_public_ip=$(terraform output k8s_lb_public_ip).Trim('"')
    $k8s_lb_sg_id=$(terraform output k8s_lb_sg_id).Trim('"')
    Set-EnvMapValue -Map $resources -Key "hw.k8s.lb_id" -Value $k8s_lb_id
    Set-EnvMapValue -Map $resources -Key "hw.k8s.lb_private_ip" -Value $k8s_lb_private_ip
    Set-EnvMapValue -Map $resources -Key "hw.k8s.lb_public_ip" -Value $k8s_lb_public_ip
    Set-EnvMapValue -Map $resources -Key "hw.k8s.lb_sg_id" -Value $k8s_lb_sg_id

    Set-Location "$PSScriptRoot/.."

    # Save terraform state to config folder
    Save-State -Component "k8s_lb" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

    # Save k8s loadbalancer ip to config file
    Set-EnvMapValue -Map $config -Key "k8s.loadbalancer.host" -Value $k8s_lb_private_ip
    Write-EnvResources -ResourcePath $ConfigPath -Resources $config
    # $configOnprem = Get-Content -Path $ConfigPath | Out-String | ConvertFrom-Json
    # $configOnprem.k8s.loadbalancer.host = $k8s_lb_private_ip
    # ConvertTo-Json $configOnprem | Set-Content -Path $ConfigPath
}
###################################################################

############################# Management Station ######################################
if (Get-EnvMapValue -Map $config -Key "hw.mgmtstation.create") {
    # Initialize terraform
    Set-Location $mgmtTerraformPath
    Write-Host "Initializing terraform..."
    terraform init
    # Check for error
    if ($LastExitCode -ne 0) {
        Set-Location "$PSScriptRoot/.."
        Write-EnvError -Component "mgmtstation" "Can't initialize terraform. Watch logs above or check '$mgmtTerraformPath' folder content."
    }

    # Plan cloud resources creation
    Write-Host "Executing terraform plan command..."
    terraform plan
    # Check for error
    if ($LastExitCode -ne 0) {
        Set-Location "$PSScriptRoot/.."
        Write-EnvError -Component "mgmtstation" "Can't execute terraform plan. Watch logs above or check '$mgmtTerraformPath' folder content."
    }

    # Create cloud resources
    Write-Host "Creating cloud resources by terraform..."
    terraform apply -auto-approve
    # Check for error
    if ($LastExitCode -ne 0) {
        Set-Location "$PSScriptRoot/.."
        Write-EnvError -Component "mgmtstation" "Can't create cloud resources. Watch logs above or check '$mgmtTerraformPath' folder content."
    }
        
    # Get public and private ip
    $mgmt_id=$(terraform output mgmt_id).Trim('"') 
    $mgmt_private_ip=$(terraform output mgmt_private_ip).Trim('"')
    $mgmt_public_ip=$(terraform output mgmt_public_ip).Trim('"')
    $mgmt_sg_id=$(terraform output mgmt_sg_id).Trim('"')
    # $mgmt_subnet_id=$(terraform output mgmt_subnet_id).Trim() -replace "`n" -replace "`""
    Set-EnvMapValue -Map $resources -Key "hw.mgmtstation.id" -Value $mgmt_id
    Set-EnvMapValue -Map $resources -Key "hw.mgmtstation.private_ip" -Value $mgmt_private_ip
    Set-EnvMapValue -Map $resources -Key "hw.mgmtstation.public_ip" -Value $mgmt_public_ip
    Set-EnvMapValue -Map $resources -Key "hw.mgmtstation.sg_id" -Value $mgmt_sg_id

    Set-Location "$PSScriptRoot/.."

    # Save terraform state to config folder
    Save-State -Component "mgmtstation" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
        
    # Wait for vm to boot
    Write-Host "Waiting 2 minutes for mgmt station to boot..."
    Start-Sleep -Seconds 120

    # Copy whole project to mgmt station
    # Define os type by full path
    if (Get-EnvMapValue -Map $config -Key "hw.mgmtstation.copy_project_to_mgmt_station") {
        $keypairName = Get-EnvMapValue -Map $resources -Key "hw.mgmtstation.keypair_name"
        $keypairPath = "$PSScriptRoot/../config/$keypair.pem"
        $mgmtUser = Get-EnvMapValue -Map $config -Key "hw.mgmtstation.username"
        $mgmtIp = Get-EnvMapValue -Map $resources -Key "hw.mgmtstation.public_ip"
        
        # Duplicate project folder and empty temp before running scp
        Copy-Item -Path "$PSScriptRoot/../" -Destination "$PSScriptRoot/../../temp/env-onprem-ps/" -Recurse
        Get-ChildItem "$PSScriptRoot/../../temp/env-onprem-ps/temp/*" | Remove-Item -Force -Recurse 

        Write-Host "Copying environment management project to mgmt station..."

        if($IsMacOS) {
            # macos
            ssh -o StrictHostKeyChecking=accept-new "$($mgmtUser)@$($mgmtIp)" -i "$keypairPath" `
                "mkdir -p /home/$($mgmtUser)"
            Set-Location "$PSScriptRoot/.."
            $tmp = (Get-Item -Path ".\").FullName
            $null = scp -i "$keypairPath" -r $tmp "$($mgmtUser)@$($mgmtIp):/home/$($mgmtUser)"
        }
        else {
            if ($PSScriptRoot[0] -eq "/") {
                # ubuntu
                ssh -o StrictHostKeyChecking=accept-new "$($mgmtUser)@$($mgmtIp)" -i "$keypairPath" `
                    "mkdir -p /home/$($mgmtUser)"
                $null = scp -i "$keypairPath" -r "$PSScriptRoot/.." `
                    "$($mgmtUser)@$($mgmtIp):/home/$($mgmtUser)"
            } else {
                # windows
                ssh -o StrictHostKeyChecking=accept-new "$($mgmtUser)@$($mgmtIp)" -i "$keypairPath" `
                    "mkdir -p /home/$($mgmtUser)/env-onprem-ps"
                $null = scp -i "$keypairPath" -r "$PSScriptRoot/../../temp/env-onprem-ps/*" `
                    "$($mgmtUser)@$($mgmtIp):/home/$($mgmtUser)/env-onprem-ps"
            }
        }
        # Cleanup copy of project in temp
        Remove-Item -LiteralPath "$PSScriptRoot/../../temp/" -Force -Recurse
        Write-Host "Done copying."

        # Check for error
        if ($LastExitCode -ne 0) {
            Write-EnvError -Component "mgmtstation" "Can't copy project to mgmt station. Read logs above..."
        }

        Set-EnvMapValue -Map $resources -Key "hw.mgmtstation.ssh_cmd" -Value "ssh $($mgmtUser)@$($mgmtIp) -i $keypairPath"
        Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
        Write-Host "To continue environment creation on mgmt station use commands:"
        $sshcmd=(Get-EnvMapValue -Map $resources -Key "hw.mgmtstation.ssh_cmd")
        Write-Host $sshcmd -ForegroundColor Green
        Write-Host "cd ~/env-onprem-ps" 

        # Set permissions to protected key
        ssh "$($mgmtUser)@$($mgmtIp)" -i "$keypairPath" `
            "chmod 600 /home/$($mgmtUser)/env-onprem-ps/config/$($keypairName).pem"
        # Set permissions to scripts
        ssh "$($mgmtUser)@$($mgmtIp)" -i "$keypairPath" `
            "chmod +x /home/$($mgmtUser)/env-onprem-ps/*.sh /home/$($mgmtUser)/env-onprem-ps/*.ps1"
    }
}
###################################################################
