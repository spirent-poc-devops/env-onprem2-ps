#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

################################## Kubernetes Virtual Machines #################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Delete aws k8s resources
Set-Location $k8sTerraformPath
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't initialize terraform. Watch logs above or check '$k8sTerraformPath' folder content."
}

terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't execute terraform plan. Watch logs above or check '$k8sTerraformPath' folder content."
}

terraform destroy -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't delete cloud resources. Watch logs above or check '$k8sTerraformPath' folder content."
}

Set-Location "$PSScriptRoot/.."

# Delete terraform state from config folder
Remove-State -Component "k8s" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

if (Test-EnvMapValue -Map $resources -Key "hw.k8s") {
    $k8sVMCount = $(Get-EnvMapValue -Map $config -Key "hw.k8s.nodes_count") + $(Get-EnvMapValue -Map $config -Key "hw.k8s.masters_count")
    for ($i = 0; $i -lt $k8sVMCount; $i++) {
        # Delete results and save resource file to disk
        Remove-EnvMapValue -Map $resources -Key "hw.k8s.$i`_private_ip"
        Remove-EnvMapValue -Map $resources -Key "hw.k8s.$i`_public_ip"
        Remove-EnvMapValue -Map $resources -Key "hw.k8s.$i`_id"
        Remove-EnvMapValue -Map $resources -Key "hw.k8s.$i`_sg_id"
        
    }
    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
}

# Delete k8s nodes ip from config file
Remove-EnvMapValue -Map $config -Key "k8s.username"
Remove-EnvMapValue -Map $config -Key "k8s.private_key_path"
Remove-EnvMapValue -Map $config -Key "k8s.master_ips"
Remove-EnvMapValue -Map $config -Key "k8s.worker_ips"
Write-EnvResources -ResourcePath $ConfigPath -Resources $config
# $configOnprem = Get-Content -Path $ConfigPath | Out-String | ConvertFrom-Json 
# $configOnprem.k8s."username" = ""
# $configOnprem.k8s."private_key_path" = ""
# $configOnprem.k8s."master_ips" = @()
# $configOnprem.k8s."worker_ips" = @()
# ConvertTo-Json $configOnprem | Set-Content -Path $ConfigPath
###################################################################

################################### k8s loadbalancer ################################
if ($(Get-EnvMapValue -Map $config -Key "hw.k8s.masters_count") -gt 1) {
    # Prepare terraform files
    $k8sLBTerraformPath = "$PSScriptRoot/../temp/k8s_lb_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
    # Prepare terraform files and set terraform env vars
    . "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

    # Delete aws k8s resources
    Set-Location $k8sLBTerraformPath
    terraform init
    # Check for error
    if ($LastExitCode -ne 0) {
        Set-Location "$PSScriptRoot/.."
        Write-EnvError -Component "kubernetes" "Can't initialize terraform. Watch logs above or check '$k8sLBTerraformPath' folder content."
    }

    terraform plan
    # Check for error
    if ($LastExitCode -ne 0) {
        Set-Location "$PSScriptRoot/.."
        Write-EnvError -Component "kubernetes" "Can't execute terraform plan. Watch logs above or check '$k8sLBTerraformPath' folder content."
    }

    terraform destroy -auto-approve
    # Check for error
    if ($LastExitCode -ne 0) {
        Set-Location "$PSScriptRoot/.."
        Write-EnvError -Component "kubernetes" "Can't delete cloud resources. Watch logs above or check '$k8sLBTerraformPath' folder content."
    }

    Set-Location "$PSScriptRoot/.."

    # Delete terraform state from config folder
    Remove-State -Component "k8s_lb" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

    if (Test-EnvMapValue -Map $resources -Key "hw.k8s") {
        Remove-EnvMapValue -Map $resources -Key "hw.k8s.lb_state"
        Remove-EnvMapValue -Map $resources -Key "hw.k8s.lb_id"
        Remove-EnvMapValue -Map $resources -Key "hw.k8s.lb_private_ip"
        Remove-EnvMapValue -Map $resources -Key "hw.k8s.lb_public_ip"
        Remove-EnvMapValue -Map $resources -Key "hw.k8s.lb_sg_id"
    }
    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
}
###################################################################

################################## Management station ##################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Delete aws k8s resources
Set-Location $mgmtTerraformPath
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "mgmtstation" "Can't initialize terraform. Watch logs above or check '$mgmtTerraformPath' folder content."
}

terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "mgmtstation" "Can't execute terraform plan. Watch logs above or check '$mgmtTerraformPath' folder content."
}

terraform destroy -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "mgmtstation" "Can't delete cloud resources. Watch logs above or check '$mgmtTerraformPath' folder content."
}

Set-Location "$PSScriptRoot/.."

# Delete terraform state from config folder
Remove-State -Component "mgmtstation" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

if (Test-EnvMapValue -Map $resources -Key "hw.mgmtstation") {
    Remove-EnvMapValue -Map $resources -Key "hw.mgmtstation.private_ip"
    Remove-EnvMapValue -Map $resources -Key "hw.mgmtstation.public_ip"
    Remove-EnvMapValue -Map $resources -Key "hw.mgmtstation.id"
    Remove-EnvMapValue -Map $resources -Key "hw.mgmtstation.sg_id"
    Remove-EnvMapValue -Map $resources -Key "hw.mgmtstation.ssh_cmd"
}
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

###################################################################

################################## Keypair ##################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Delete aws k8s resources
Set-Location $keypairTerraformPath
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "keypair" "Can't initialize terraform. Watch logs above or check '$keypairTerraformPath' folder content."
}

terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "keypair" "Can't execute terraform plan. Watch logs above or check '$keypairTerraformPath' folder content."
}

terraform destroy -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "keypair" "Can't delete cloud resources. Watch logs above or check '$keypairTerraformPath' folder content."
}

Set-Location "$PSScriptRoot/.."

# Delete terraform state from config folder
Remove-State -Component "keypair" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

if (Test-EnvMapValue -Map $resources -Key "hw.mgmtstation") {
    Remove-EnvMapValue -Map $resources -Key "hw.mgmtstation.keypair_name"
}
if (Test-EnvMapValue -Map $resources -Key "hw.k8s") {
    Remove-EnvMapValue -Map $resources -Key "hw.k8s.keypair_name"
}
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

###################################################################