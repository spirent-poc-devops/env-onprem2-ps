#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 2)]
    [hashtable] $Resources
)

# Save env_nameprefix without dashes (terraform suggests use resource names without dashes)
Set-EnvMapValue -Map $resources -Key "environment.prefix" -Value $($(Get-EnvMapValue -Map $config -Key "environment.prefix").replace("-","_"))

# # Configure AWS cli
# $env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "hw.aws.region"
# $env:TF_VAR_account_id = Get-EnvMapValue -Map $config -Key "hw.aws.account_id"

# Prepare terraform files

############################## Keypair #####################################
$keypairTerraformPath = "$PSScriptRoot/../temp/keypair_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
if (Test-Path $keypairTerraformPath) {
    Remove-Item $keypairTerraformPath -Recurse -Force
} 
$null = New-Item $keypairTerraformPath -ItemType "directory"

# Select cloud terraform templates
switch (Get-EnvMapValue -Map $config -Key "hw.cloud") {
    "aws" {
        # Select cloud terraform templates/aws
        $env:TF_VAR_aws_cred_file = Get-EnvMapValue -Map $config -Key "hw.aws.credentials_file"
        $env:TF_VAR_aws_profile = Get-EnvMapValue -Map $config -Key "hw.aws.profile"
        $env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "hw.aws.region"
        $env:TF_VAR_aws_stage = Get-EnvMapValue -Map $config -Key "hw.aws.stage"
        $env:TF_VAR_aws_namespace = Get-EnvMapValue -Map $config -Key "hw.aws.namespace"
        $env:TF_VAR_env_nameprefix = Get-EnvMapValue -Map $config -Key "environment.prefix"

        Copy-Item -Path "$PSScriptRoot/templates/aws/provider.tf" -Destination "$keypairTerraformPath/provider.tf"
        Copy-Item -Path "$PSScriptRoot/templates/aws/keypair.tf" -Destination "$keypairTerraformPath/keypair.tf"

        # Load terraform state from config folder
        Sync-State -Component "keypair" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")
    }
    DEFAULT {
        Write-EnvError -Component "kubernetes" "Cloud $(Get-EnvMapValue -Map $config -Key "hw.cloud") doesn't supported."
    }
}
###################################################################

################################## Kubernetes Virtual Machines #################################
$k8sTerraformPath = "$PSScriptRoot/../temp/k8s_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
if (Test-Path $k8sTerraformPath) {
    Remove-Item $k8sTerraformPath -Recurse -Force
} 
$null = New-Item $k8sTerraformPath -ItemType "directory"

# Select cloud terraform templates
switch (Get-EnvMapValue -Map $config -Key "hw.cloud") {
    "aws" {
        $env:TF_VAR_aws_cred_file = Get-EnvMapValue -Map $config -Key "hw.aws.credentials_file"
        $env:TF_VAR_aws_profile = Get-EnvMapValue -Map $config -Key "hw.aws.profile"
        $env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "hw.aws.region"
        $env:TF_VAR_k8s_instance_ami = Get-EnvMapValue -Map $config -Key "hw.k8s.instance_ami"
        $env:TF_VAR_k8s_instance_type = Get-EnvMapValue -Map $config -Key "hw.k8s.instance_type"
        $env:TF_VAR_k8s_keypair_name = Get-EnvMapValue -Map $resources -Key "hw.k8s.keypair_name"
        $env:TF_VAR_k8s_subnet_id = Get-EnvMapValue -Map $config -Key "hw.k8s.subnet_id"
        $env:TF_VAR_env_nameprefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
        $env:TF_VAR_aws_vpc = Get-EnvMapValue -Map $config -Key "hw.aws.vpc"
        $env:TF_VAR_mgmt_subnet_cidr = Get-EnvMapValue -Map $config -Key "hw.mgmtstation.subnet_cidr"
        # $env:TF_VAR_mgmt_public_ip = Get-EnvMapValue -Map $resources -Key "mgmtstation.public_ip"
        $env:TF_VAR_k8s_subnet_cidr = Get-EnvMapValue -Map $config -Key "hw.k8s.subnet_cidr"
        $env:TF_VAR_mgmt_public_ip = Get-EnvMapValue -Map $resources -Key "hw.mgmtstation.public_ip"
        $env:TF_VAR_k8s_volume_size = Get-EnvMapValue -Map $config -Key "hw.k8s.volume_size"
        $env:TF_VAR_k8s_associate_public_ip_address = Get-EnvMapValue -Map $config -Key "hw.k8s.associate_public_ip_address"
        $env:TF_VAR_k8s_ebs_snapshot_id = Get-EnvMapValue -Map $config -Key "hw.k8s.ebs_snapshot_id"
    }
    DEFAULT {
        Write-EnvError -Component "kubernetes" "Cloud $(Get-EnvMapValue -Map $config -Key "hw.cloud") doesn't supported."
    }
}

$k8sVMCount = $(Get-EnvMapValue -Map $config -Key "hw.k8s.nodes_count") + $(Get-EnvMapValue -Map $config -Key "hw.k8s.masters_count")
for ($i = 0; $i -lt $k8sVMCount; $i++) {

    $templateParams = @{ 
        k8s_vm_index=$i; 
    }
    
    Build-EnvTemplate -InputPath "$($PSScriptRoot)/templates/aws/k8s_vm.tf" -OutputPath "$k8sTerraformPath/k8s_vm_$i.tf" -Params1 $templateParams
}

Copy-Item -Path "$PSScriptRoot/templates/aws/k8s_variables.tf" -Destination "$k8sTerraformPath/k8s_variables.tf"
Copy-Item -Path "$PSScriptRoot/templates/aws/provider.tf" -Destination "$k8sTerraformPath/provider.tf"

# Load terraform state from config folder
Sync-State -Component "k8s" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")
###################################################################

############################### k8s loadbalancer ####################################
if ($(Get-EnvMapValue -Map $config -Key "hw.k8s.masters_count") -gt 1) {
    # Prepare terraform files
    $k8sLBTerraformPath = "$PSScriptRoot/../temp/k8s_lb_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
    if (Test-Path $k8sLBTerraformPath) {
        Remove-Item $k8sLBTerraformPath -Recurse -Force
    } 
    $null = New-Item $k8sLBTerraformPath -ItemType "directory"

    # Select cloud terraform templates
    switch (Get-EnvMapValue -Map $config -Key "hw.cloud") {
        "aws" {
            $env:TF_VAR_aws_cred_file = Get-EnvMapValue -Map $config -Key "hw.aws.credentials_file"
            $env:TF_VAR_aws_profile = Get-EnvMapValue -Map $config -Key "hw.aws.profile"
            $env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "hw.aws.region"
            $env:TF_VAR_k8s_lb_instance_ami = Get-EnvMapValue -Map $config -Key "hw.k8s.instance_ami"
            $env:TF_VAR_k8s_lb_instance_type = Get-EnvMapValue -Map $config -Key "hw.k8s.instance_type"
            $env:TF_VAR_k8s_lb_subnet_id = Get-EnvMapValue -Map $config -Key "hw.k8s.subnet_id"
            $env:TF_VAR_env_nameprefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
            $env:TF_VAR_aws_vpc = Get-EnvMapValue -Map $config -Key "hw.aws.vpc"
            $env:TF_VAR_mgmt_subnet_cidr = Get-EnvMapValue -Map $config -Key "hw.mgmtstation.subnet_cidr"
            $env:TF_VAR_k8s_subnet_cidr = Get-EnvMapValue -Map $config -Key "hw.k8s.subnet_cidr"
            }
        DEFAULT {
            Write-EnvError -Component "k8s_lb" "Cloud $(Get-EnvMapValue -Map $config -Key "hw.cloud") doesn't supported."
        }
    }

    Copy-Item -Path "$PSScriptRoot/templates/aws/provider.tf" -Destination "$k8sLBTerraformPath/provider.tf"
    Copy-Item -Path "$PSScriptRoot/templates/aws/k8s_lb.tf" -Destination "$k8sLBTerraformPath/k8s_lb.tf"

    # Load terraform state from config folder
    Sync-State -Component "k8s_lb" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")
}
###################################################################

############################# Management Station ######################################
# Prepare terraform files
$mgmtTerraformPath = "$PSScriptRoot/../temp/mgmtstation_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
if (Test-Path $mgmtTerraformPath) {
    Remove-Item $mgmtTerraformPath -Recurse -Force
} 
$null = New-Item $mgmtTerraformPath -ItemType "directory"

# Select cloud terraform templates
switch (Get-EnvMapValue -Map $config -Key "hw.cloud") {
    "aws" {
        $env:TF_VAR_aws_cred_file = Get-EnvMapValue -Map $config -Key "hw.aws.credentials_file"
        $env:TF_VAR_aws_profile = Get-EnvMapValue -Map $config -Key "hw.aws.profile"
        $env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "hw.aws.region"
        $env:TF_VAR_mgmt_instance_ami = Get-EnvMapValue -Map $config -Key "hw.mgmtstation.instance_ami"
        $env:TF_VAR_mgmt_instance_type = Get-EnvMapValue -Map $config -Key "hw.mgmtstation.instance_type"
        $env:TF_VAR_mgmt_keypair_name = Get-EnvMapValue -Map $resources -Key "hw.mgmtstation.keypair_name"
        $env:TF_VAR_mgmt_subnet_id = Get-EnvMapValue -Map $config -Key "hw.mgmtstation.subnet_id"
        $env:TF_VAR_env_nameprefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
        $env:TF_VAR_aws_vpc = Get-EnvMapValue -Map $config -Key "hw.aws.vpc"
        $env:TF_VAR_mgmt_ssh_allowed_cidr_blocks = Get-EnvMapValue -Map $config -Key "hw.mgmtstation.ssh_allowed_cidr_blocks"
        $env:TF_VAR_mgmt_volume_size = Get-EnvMapValue -Map $config -Key "hw.mgmtstation.volume_size"
    }
    DEFAULT {
        Write-EnvError -Component "mgmtstation" "Cloud $(Get-EnvMapValue -Map $config -Key "hw.cloud") doesn't supported."
    }
}

Copy-Item -Path "$PSScriptRoot/templates/aws/provider.tf" -Destination "$mgmtTerraformPath/provider.tf"
Copy-Item -Path "$PSScriptRoot/templates/aws/mgmtstation.tf" -Destination "$mgmtTerraformPath/mgmtstation.tf"

# Load terraform state from config folder
Sync-State -Component "mgmtstation" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")
###################################################################