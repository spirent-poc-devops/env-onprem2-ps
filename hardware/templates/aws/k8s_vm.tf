# Configure AWS instance
resource "aws_instance" "k8s_<%=k8s_vm_index%>" {
  ami                         = var.k8s_instance_ami
  instance_type               = var.k8s_instance_type
  key_name                    = var.k8s_keypair_name
  associate_public_ip_address = var.k8s_associate_public_ip_address
  subnet_id                   = var.k8s_subnet_id
  vpc_security_group_ids      = [aws_security_group.k8s_<%=k8s_vm_index%>.id]
  ebs_block_device {
    device_name = "/dev/sda1"
    volume_size = var.k8s_volume_size
    snapshot_id = var.k8s_ebs_snapshot_id
  }

  tags = {
    Environment = var.env_nameprefix
    Name        = "k8s_<%=k8s_vm_index%>_${var.env_nameprefix}"
  }
}

# Configure AWS security group
resource "aws_security_group" "k8s_<%=k8s_vm_index%>" {
  name        = "k8s_<%=k8s_vm_index%>_${var.env_nameprefix}"
  description = "k8s_<%=k8s_vm_index%> access"
  vpc_id      = var.aws_vpc

  ingress {
    description = "access_from_mgmt"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["${var.mgmt_subnet_cidr}"]
#    cidr_blocks = ["${var.mgmt_subnet_cidr}","${var.mgmt_public_ip}/32"]
  }

  ingress {
    description = "access_bw_k8s_nodes"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = [var.k8s_subnet_cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = var.env_nameprefix
    Name        = "k8s_<%=k8s_vm_index%>_<%=env_nameprefix%>_sg"
  }
}

# Save k8s instance id
output "k8s_<%=k8s_vm_index%>_id" {
  value       = aws_instance.k8s_<%=k8s_vm_index%>.id
  description = "The unique identifier of the k8s virtual machine <%=k8s_vm_index%>."
}

# Save k8s security group id
output "k8s_<%=k8s_vm_index%>_sg_id" {
  value       = aws_security_group.k8s_<%=k8s_vm_index%>.id
  description = "The unique identifier of the k8s security group <%=k8s_vm_index%>."
}

# Save k8s instance private ip
output "k8s_<%=k8s_vm_index%>_private_ip" {
  value       = aws_instance.k8s_<%=k8s_vm_index%>.private_ip
  description = "The private IP address of the k8s virtual machine <%=k8s_vm_index%>."
}

# Save k8s instance public ip
output "k8s_<%=k8s_vm_index%>_public_ip" {
  value       = aws_instance.k8s_<%=k8s_vm_index%>.public_ip
  description = "The public IP address of the k8s virtual machine <%=k8s_vm_index%>."
}
