variable "mgmt_instance_ami" {
  type        = string
  description = "Management station instance ami"
}

variable "mgmt_instance_type" {
  type        = string
  description = "Management station instance type"
}

variable "mgmt_keypair_name" {
  type        = string
  description = "Management station instance keypair name"
}

variable "mgmt_subnet_id" {
  type        = string
  description = "Management station subnet id"
}

variable "env_nameprefix" {
  type        = string
  description = "Environment prefix used for naming resources"
}

variable "aws_vpc" {
  type        = string
  description = "Management station AWS vpc"
}

variable "mgmt_volume_size" {
  type        = string
  description = "Management station instance volume size"
}

variable "mgmt_ssh_allowed_cidr_blocks" {
  type        = string
  description = "CIDR of ips allowed to connect to the management station"
}

# Configure AWS instance
resource "aws_instance" "mgmt" {
  ami                         = var.mgmt_instance_ami
  instance_type               = var.mgmt_instance_type
  key_name                    = var.mgmt_keypair_name
  associate_public_ip_address = true
  subnet_id                   = var.mgmt_subnet_id
  vpc_security_group_ids      = [aws_security_group.mgmt.id]
  ebs_block_device {
    device_name = "/dev/sda1"
    volume_size = var.mgmt_volume_size
  }

  tags = {
    Environment = var.env_nameprefix
    Name        = "${var.env_nameprefix}_mgmt"
  }
}

# Configure AWS security group
resource "aws_security_group" "mgmt" {
  name        = "${var.env_nameprefix}_mgmt"
  description = "Management station access"
  vpc_id      = var.aws_vpc

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.mgmt_ssh_allowed_cidr_blocks]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = var.env_nameprefix
    Name        = "${var.env_nameprefix}_mgmt_sg"
  }
}

# Save mgmt instance id
output "mgmt_id" {
  value       = aws_instance.mgmt.id
  description = "The unique identifier of the management station virtual machine."
}

# Save mgmt security group id
output "mgmt_sg_id" {
  value       = aws_security_group.mgmt.id
  description = "The unique identifier of the management station security group."
}

# Save mgmt instance private ip
output "mgmt_private_ip" {
  value       = aws_instance.mgmt.private_ip
  description = "The private IP address of the management station virtual machine."
}

# Save mgmt instance public ip
output "mgmt_public_ip" {
  value       = aws_instance.mgmt.public_ip
  description = "The public IP address of the management station virtual machine."
}
