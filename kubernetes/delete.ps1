#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath


# Prepare hosts file if missing
if (!(Test-Path -Path "$PSScriptRoot/../temp/k8s_ansible_hosts")) {
    $ansibleInventory = @("[master_init]")
    $ansibleInventory += "master_init ansible_host=$($(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.master_ips")[0]) ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.private_key_path")"
    
    $ansibleInventory += "`r`n[masters_redundant]"
    $i = 0
    foreach ($node in $(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.master_ips" | select -skip 1)) {
        $ansibleInventory += "master_redundant$i ansible_host=$node ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.private_key_path")"
        $i++
    }

    $ansibleInventory += "`r`n[workers]"
    $i = 0
    foreach ($node in (Get-EnvMapValue -Map $config -Key "$ConfigPrefix.worker_ips")) {
        $ansibleInventory += "worker$i ansible_host=$node ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.private_key_path")"
        $i++
    }

    Set-Content -Path "$PSScriptRoot/../temp/k8s_ansible_hosts" -Value $ansibleInventory
}

# Whitelist nodes
if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/templates/ssh_keyscan_playbook.yml"
} else {
    ansible-playbook -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/templates/ssh_keyscan_playbook.yml"
}

# Destroy k8s cluster
Build-EnvTemplate -InputPath "$($PSScriptRoot)/templates/k8s_uninstall_playbook.yml" -OutputPath "$($PSScriptRoot)/../temp/k8s_uninstall_playbook.yml" -Params1 $templateParams

if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_uninstall_playbook.yml"
} else {
    ansible-playbook -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_uninstall_playbook.yml"
}

# Remove k8s cluster from mgmt station .kube/config
$envPrefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
$k8sUsername = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.username"

# Remove k8s cluster from config
if (Test-Path "/home/$k8sUsername/.kube/config") {
    Write-EnvInfo -Component "kubernetes" "Removing $envPrefix k8s cluster from mgmt station .kube/config"

    kubectl config unset "users.$envPrefix-k8s-admin"
    kubectl config unset "contexts.$envPrefix-k8s"
    kubectl config unset "clusters.$envPrefix-k8s"
    $mgmtConfigText = Get-Content "/home/$k8sUsername/.kube/config"
    $mgmtConfigText -replace "null","" | Set-Content "/home/$k8sUsername/.kube/config"

    # Remove env config
    Remove-Item -Path "/home/$k8sUsername/.kube/config_$envPrefix" -Force
}

# Delete results and save resource file to disk
if (Test-EnvMapValue -Map $resources -Key "k8s") {
    Remove-EnvMapValue -Map $resources -Key "k8s.type"
    Remove-EnvMapValue -Map $resources -Key "k8s.state"
    Remove-EnvMapValue -Map $resources -Key "k8s.inventory"
    Remove-EnvMapValue -Map $resources -Key "k8s.namespace"
}

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
