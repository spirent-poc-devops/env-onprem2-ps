#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath


# Prepare hosts file
$ansibleInventory = @("[master_init]")

if ($(Get-EnvMapValue -Map $config -Key "k8s.master_ips").Count -gt 1) {
    # Initial master
    $ansibleInventory += "master_init ansible_host=$($(Get-EnvMapValue -Map $config -Key "k8s.master_ips")[0]) ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "k8s.username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "k8s.private_key_path")"

    # Redundant masters
    $ansibleInventory += "`r`n[masters_redundant]"
    $i = 0
    foreach ($node in $(Get-EnvMapValue -Map $config -Key "k8s.master_ips" | select -skip 1)) {
        $ansibleInventory += "master_redundant$i ansible_host=$node ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "k8s.username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "k8s.private_key_path")"
        $i++
    }

    # Loadbalancer
    $ansibleInventory += "`r`n[lb]"
    if (Get-EnvMapValue -Map $config -Key "k8s.loadbalancer.haproxy") {
        $ansibleInventory += "lb0 ansible_host=$(Get-EnvMapValue -Map $config -Key "k8s.loadbalancer.host") ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "k8s.username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "k8s.private_key_path")"
        }
    } else {
    # single master
    $ansibleInventory += "master_init ansible_host=$(Get-EnvMapValue -Map $config -Key "k8s.master_ips") ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "k8s.username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "k8s.private_key_path")"
}

$ansibleInventory += "`r`n[workers]"
$i = 0
foreach ($node in (Get-EnvMapValue -Map $config -Key "k8s.worker_ips")) {
    $ansibleInventory += "worker$i ansible_host=$node ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "k8s.username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "k8s.private_key_path")"
    $i++
}
Set-Content -Path "$PSScriptRoot/../temp/k8s_ansible_hosts" -Value $ansibleInventory

# Whitelist nodes
if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/templates/ssh_keyscan_playbook.yml"
} else {
    ansible-playbook -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/templates/ssh_keyscan_playbook.yml"
}

# Install k8s dependecies
$k8s_cni_version = Get-EnvMapValue -Map $config -Key "k8s.cni_version"
$k8s_crictl_version = Get-EnvMapValue -Map $config -Key "k8s.crictl_version"
$k8s_kubelet_version = Get-EnvMapValue -Map $config -Key "k8s.kubelet_version"
$k8s_kubeadm_version = Get-EnvMapValue -Map $config -Key "k8s.kubeadm_version"
$k8s_kubectl_version = Get-EnvMapValue -Map $config -Key "k8s.kubectl_version"

$templateParams = @{ 
    k8s_cni_version=$k8s_cni_version; 
    k8s_crictl_version=$k8s_crictl_version; 
    k8s_kubelet_version=$k8s_kubelet_version; 
    k8s_kubeadm_version=$k8s_kubeadm_version; 
    k8s_kubectl_version=$k8s_kubectl_version 

}
Build-EnvTemplate -InputPath "$($PSScriptRoot)/templates/k8s_prerequisites_playbook.yml" -OutputPath "$($PSScriptRoot)/../temp/k8s_prerequisites_playbook.yml" -Params1 $templateParams

if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_prerequisites_playbook.yml"
} else {
    ansible-playbook -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_prerequisites_playbook.yml"
}

if ($(Get-EnvMapValue -Map $config -Key "k8s.master_ips").Count -gt 1) {
    # Multimaster cluster

    # Configure loadbalancer
    if (Get-EnvMapValue -Map $config -Key "k8s.loadbalancer.haproxy") {
        $haproxy_masters_endpoints = ""
        foreach($masterIp in Get-EnvMapValue -Map $config -Key "k8s.master_ips") {
            $haproxy_masters_endpoints += "                server master1 $($masterIp):6443 check`n"
        }
        $templateParams = @{ 
            haproxy_masters_endpoints=$haproxy_masters_endpoints
        }
        Build-EnvTemplate -InputPath "$($PSScriptRoot)/templates/k8s_lb_playbook.yml" -OutputPath "$($PSScriptRoot)/../temp/k8s_lb_playbook.yml" -Params1 $templateParams
        if ($config["extended_ansible_logs"]) {
            ansible-playbook -v -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_lb_playbook.yml"
        } else {
            ansible-playbook -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_lb_playbook.yml"
        }
    }

    # Init k8s cluster on master node
    $env_prefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
    $k8s_network = Get-EnvMapValue -Map $config -Key "k8s.network"
    $k8s_username = Get-EnvMapValue -Map $config -Key "k8s.username"
    $lb_host = Get-EnvMapValue -Map $config -Key "k8s.loadbalancer.host"
    $lb_port = Get-EnvMapValue -Map $config -Key "k8s.loadbalancer.port"

    $templateParams = @{ 
        env_prefix=$env_prefix;
        k8s_network=$k8s_network;
        k8s_username=$k8s_username;
        lb_host=$lb_host;
        lb_port=$lb_port;
    }

    Build-EnvTemplate -InputPath "$($PSScriptRoot)/templates/k8s_master_init_playbook.yml" -OutputPath "$($PSScriptRoot)/../temp/k8s_master_init_playbook.yml" -Params1 $templateParams
    
    if ($config["extended_ansible_logs"]) {
        ansible-playbook -v -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_master_init_playbook.yml"
    } else {
        ansible-playbook -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_master_init_playbook.yml"
    }

    # Initialize redundant masters
    $k8s_network = Get-EnvMapValue -Map $config -Key "k8s.network"
    $k8s_username = Get-EnvMapValue -Map $config -Key "k8s.username"

    $templateParams = @{
        k8s_network=$k8s_network;
        k8s_username=$k8s_username;
    }

    Build-EnvTemplate -InputPath "$($PSScriptRoot)/templates/k8s_master_redundant_playbook.yml" -OutputPath "$($PSScriptRoot)/../temp/k8s_master_redundant_playbook.yml" -Params1 $templateParams
    
    if ($config["extended_ansible_logs"]) {
        ansible-playbook -v -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_master_redundant_playbook.yml"
    } else {
        ansible-playbook -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_master_redundant_playbook.yml"
    }

} else {
    # Single master cluster
    $env_prefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
    $k8s_network = Get-EnvMapValue -Map $config -Key "k8s.network"
    $k8s_username = Get-EnvMapValue -Map $config -Key "k8s.username"

    $templateParams = @{ 
        env_prefix=$env_prefix;
        k8s_network=$k8s_network;
        k8s_username=$k8s_username;
    }

    Build-EnvTemplate -InputPath "$($PSScriptRoot)/templates/k8s_master_single_playbook.yml" -OutputPath "$($PSScriptRoot)/../temp/k8s_master_single_playbook.yml" -Params1 $templateParams
    
    if ($config["extended_ansible_logs"]) {
        ansible-playbook -v -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_master_single_playbook.yml"
    } else {
        ansible-playbook -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_master_single_playbook.yml"
    }
}

# Join workers to master
Build-EnvTemplate -InputPath "$($PSScriptRoot)/templates/k8s_worker_playbook.yml" -OutputPath "$($PSScriptRoot)/../temp/k8s_worker_playbook.yml" -Params1 $templateParams

if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_worker_playbook.yml"
} else {
    ansible-playbook -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_worker_playbook.yml"
}

if ($LastExitCode -ne 0) {
    Write-EnvError -Component "kubernetes" "Error on installing k8s cluster. See logs above"
}

# Replace k8s cluster name in new kube config
(Get-Content -Path "/home/$k8s_username/.kube/config_$env_prefix" -Raw)  -replace "kubernetes-admin@kubernetes","$env_prefix-k8s" -replace "kubernetes","$env_prefix-k8s" | Set-Content "/home/$k8s_username/.kube/config_$env_prefix"

if (!(Test-Path -Path "/home/$k8s_username/.kube/config") -or [String]::IsNullOrWhiteSpace((Get-content "/home/$k8s_username/.kube/config"))) {
    # First k8s cluster created from mgmt station. Just copy config
    Get-Content -Path "/home/$k8s_username/.kube/config_$env_prefix" | Add-Content -Path "/home/$k8s_username/.kube/config"
} else {
    # Verify that cluster not allready in .kube/config (in case of update)
    $clusters = kubectl config get-contexts -o name
    if ($clusters -NotContains "$env_prefix-k8s") {
        # Add new cluster to mgmt station kube/config
        $mgmtConfigText = Get-Content "/home/$k8s_username/.kube/config"
        $newEnvConfigText = Get-Content "/home/$k8s_username/.kube/config_$env_prefix"

        $newMgmtConfigText = @() 

        # indexes are default for kubeadm configs
        $newCluserSection = $newEnvConfigText[2..5]
        $newContextSection = $newEnvConfigText[7..10]
        $newUserSection = $newEnvConfigText[15..18]

        foreach ($Line in $mgmtConfigText) {    
            $newMgmtConfigText += $Line

            if ($Line -match "clusters:") {
                $newMgmtConfigText += $newCluserSection
            } 
            if ($Line -match "contexts:") {
                $newMgmtConfigText += $newContextSection
            } 
            if ($Line -match "users:") {
                $newMgmtConfigText += $newUserSection
            } 
        }

        Set-Content "/home/$k8s_username/.kube/config" $newMgmtConfigText
    }
}

# Write k8s resources
Set-EnvMapValue -Map $resources -Key "k8s.type" -Value "kubeadm"
Set-EnvMapValue -Map $resources -Key "k8s.inventory" -Value $ansibleInventory

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
