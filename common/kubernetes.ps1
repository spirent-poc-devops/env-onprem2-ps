function Switch-EnvKubeContext {
    [CmdletBinding()]
    param (
        [Alias("c")]
        [Parameter(Mandatory = $true, Position = 0)]
        [string] $Context
    )

    $currentContext = kubectl config current-context

    if (($currentContext -ne $null) -and ($currentContext -ne $Context)) {
        kubectl config use-context "$Context"

        if ($LastExitCode -ne 0) {
            Write-Error "There were errors switching to $Context context, Watch logs above"
            exit 0
        }
    }
}
