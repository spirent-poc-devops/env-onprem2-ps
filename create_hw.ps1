#!/usr/bin/env pwsh

param
(
    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $ConfigPath,

    [Alias("r", "Resources")]
    [Parameter(Mandatory = $false, Position = 1)]
    [string] $ResourcePath
)

# Stop on error
$ErrorActionPreference = "Stop"

# Load support functions
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }
. "$($rootPath)/common/include.ps1"
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq "")) {
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}

try{
    $totalStartTime = $(Get-Date)
    $totalStatus = "SUCCESS"
    $statuses = @()

    # Clear errors variable for clean logging
    $error.clear()

    try {
        $startTime = $(Get-Date)
        $status = "SUCCESS"

        Write-EnvInfo -Component "hardware" "Started creating hardware." -Delimiter

        . "$($rootPath)/hardware/create.ps1" -ConfigPath $ConfigPath -ResourcePath $ResourcePath

        Write-EnvInfo -Component "hardware" "Completed creating hardware." -Delimiter
    }
    # Catch and log any errors
    catch {
        $status = "FAIL"
        $totalStatus = "FAIL"

        $error
        Write-EnvError -Component "hardware" "Can't create the hardware. See logs above."
    }
    finally {
        $elapsedTime = $(Get-Date) - $startTime
        $duration = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
        $statuses += @{ Component="hardware"; Duration=$duration; Status=$status }

        if ($status -eq "FAIL") {
            Write-EnvError -Component "hardware" "hardware create $status in $duration (HH:mm:ss.ms)"
        }
        else {
            Write-EnvInfo -Component "hardware" "hardware create $status in $duration (HH:mm:ss.ms)" -Color "Yellow"
        }
    }
}
finally {
    $totalElapsedTime = $(Get-Date) - $totalStartTime
    $totalDuration = "{0:HH:mm:ss.ffff}" -f ([datetime]$totalElapsedTime.Ticks)

    Write-EnvInfo -Component "environment" "Overall create status: $totalStatus" -Color "Yellow"

    $statuses | ForEach-Object {[PSCustomObject]$_} | Format-Table -AutoSize

    Write-EnvInfo -Component "environment" "Total create duration: $totalDuration (HH:mm:ss.ms)" -Color "Yellow"
}
